def conv(OK):
    global ok
    if OK == 'Y' or OK == 'YES' or OK == 'YEA':
        ok = True
    elif OK == 'N' or OK == 'NO' or OK == 'NOPE':
        ok = False
    else:
        print('Please enter a valid response.')