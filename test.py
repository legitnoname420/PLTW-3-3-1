# This will download the playlist.

if exists('music/' + s.results[i].title + '.mp3'):
    print('Song already downloaded. Skipping to next song.')
else:
    # https://www.geeksforgeeks.org/download-video-in-mp3-format-using-pytube/ was very useful for the part below
    video = yt.streams.filter(only_audio=True).first()
    destination = 'music/'
    out_file = video.download(output_path=destination)
    base, ext = os.path.splitext(out_file)
    new_file = base + '.mp3'
    os.rename(out_file, new_file)
    # end snippet from https://www.geeksforgeeks.org/download-video-in-mp3-format-using-pytube/

title = s.results[i].title


os.system('mpv "' + './music/' + title + '.mp3"')