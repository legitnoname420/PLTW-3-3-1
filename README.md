# something

In Social Studies we have a problem with finding good music. My teacher will spend a few minutes finding a good playlist for the class. Then after a few minutes he gets bored. To help with this problem I will create a program to search for music and stream it with mpv or some python alternative. After the song is finished the script will ask the user if they liked it. If they lik_e the song it will be added to a local playlist that can be played by typing playlist.

You will need to create a nix replit and do a whole bunch to get it working on replit.


On windows I used chocolatey to install mpv
```cmd
choco install mpv
```

Install modules
```
pip install -r requirements.txt
```
