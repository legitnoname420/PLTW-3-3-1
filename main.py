# In Social Studies we have a problem with finding good music. My teacher will spend a few minutes finding a good playlist for the class. Then after a few minutes he gets bored of the music. To help with this problem I will create a program to search for music and stream/download it. After the song is finished the script will ask the user if they liked it. If they like the song it will be added to a local playlist that can be played by typing playlist.
# I am challenging myself to try to use as few external libraries as possible. I will try to only use pytube as it is more flexible as a library and something for audio.
# This may morph into a web lofi player if I keep working on it.

from pytube import Search
from pytube import YouTube
from os import name, system
from os.path import exists
import os

# This will ask the user if they want to play the song the song and convert their response to a boolean.
def ask():
    global ok
    try:
        conv(input('Is this the song you wanted? (y/n)\n').upper())
    except:
        conv(input('Is this the song you wanted? (y/n)\n').upper())

# Convert the user's response to a boolean.
def conv(OK):
    global ok
    if OK == 'Y' or OK == 'YES' or OK == 'YEA':
        ok = True
    elif OK == 'N' or OK == 'NO' or OK == 'NOPE':
        ok = False
    else:
        print('Please enter a valid response.')

# Clear Screen
def clear():

    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')
# clear screen code from https://www.geeksforgeeks.org/clear-screen-python/


# Way too much search stuff
def search():
    global searchterm, s, ok, localplaylist, file, yt, lines
    clear()
    searchterm = input('What song would you like to search for? Type "download:" to download the local playlist in case the wifi goes down. Type "playlist:shuffle" to shuffle the local playlist and start playing. Type only "playlist:" to play the local playlist without shuffling.\n')

    # Playlist Downloading
    if 'download:' in searchterm:
        conv(input('Are you sure you want to download your local playlist with ' + str(len(localplaylist)) + ' songs? (y/n)').upper())
        if ok == True:
            localplaylist = []
            with open('playlist.txt') as file:
                while line := file.readline():
                    localplaylist.append(line.rstrip())
                file.close()
        print('Downloading playlist...')
        for i in range(len(localplaylist)):
            print(localplaylist[i])
            yt = YouTube(localplaylist[i])
            if exists('music/' + yt.title + '.mp3'):
                print('Song already downloaded. Skipping to next song.')
            else:
                # https://www.geeksforgeeks.org/download-video-in-mp3-format-using-pytube/ was very hel[ful] for the part below
                video = yt.streams.filter(only_audio=True).first()
                destination = 'music/'
                out_file = video.download(output_path=destination)
                base, ext = os.path.splitext(out_file)
                new_file = base + '.mp3'
                os.rename(out_file, new_file)
                print('Finished downloading ' + yt.title + '. Video ' + str(i) + ' of ' + str(len(localplaylist)) + '.')
                # end snippet from https://www.geeksforgeeks.org/download-video-in-mp3-format-using-pytube/
        print('Download complete.')
        input('Press enter to continue.')
        clear()
        del ok, searchterm, s, yt, localplaylist, file
        search()
    # Playlist Shuffle Playing
    elif 'playlist:shuffle' in searchterm:
        lines = sum(1 for line in open('playlist.txt'))
        conv(input('Are you sure you want to play your local playlist with ' + str(lines) + ' songs? (y/n)\n').upper())
        if ok == True:
            print('Press Enter to skip to the next song or use < and > to go forward and backwards.')
            print('Playing playlist...')
    # MPV does all of the hard work but in case I am not allowed to use MPV I have a backup that uses pytube.
            os.system('mpv --shuffle --no-video --playlist=playlist.txt')
        else:
            print('See ya later, alligator')
            search()
        del ok, searchterm, s, yt, localplaylist, file
    # Playlist Playing
    elif 'playlist:' in searchterm:
        lines = sum(1 for line in open('playlist.txt'))
        conv(input('Are you sure you want to play your local playlist with ' + str(lines) + ' songs? (y/n)\n').upper())
        if ok == True:
            print('Press Enter to skip to the next song or use < and > to go forward and backwards.')
            print('Playing playlist...')
            os.system('mpv --no-video --playlist=playlist.txt')
        else:
            print('See ya later, alligator')
            search()
        del ok, searchterm, s, yt, localplaylist, file, lines
    elif 'lofi:' in searchterm:
        print('Pres q to exit. This is a livestream. You will need a stable connection to play this.')
        os.system('mpv --no-video https://youtu.be/5qap5aO4i9A')
    else:
        s = Search(searchterm)
        print(str(len(s.results)) + ' results found.')
        print(s.results[0].title)
        ask()
search()


# This is the logic behind finding a song to play.

if ok == True:
    i = 0
    print('Playing song')
elif ok == False:
    for i in range(len(s.results)):
        if ok == True:
            print('Playing song')
            break
        else:
            try:
                print('Searching for next song')
                print(s.results[i+1].title)
                ask()
            except IndexError:
                print('Sorry, I only have ' + str(len(s.results)) + ' songs to play.')
                print('Returning to search.')
                search()


# This converts the video id into a usable link.


id = s.results[i].video_id
link = 'https://youtu.be/' + id
yt = YouTube(link)
# youtu.be redirects to youtube.com, but it converts the id to a usable youtube.com link without any work on my part. ex: https://youtu.be/dQw4w9WgXcQ -> https://www.youtube.com/watch?v=dQw4w9WgXcQ

# This will play the song by passing the video link to mpv with the "--no-video" flag.
print('Press q to stop playing.')
os.system('mpv --no-video ' + link)

# This will ask the user if they liked the song and converts their response to a boolean.
conv(input('Would you like to add this song to your playlist? (y/n)\n').upper())
if ok == True:
    with open('playlist.txt', 'a') as playlist:
        playlist.write(link + '\n')
    playlist.close()
    search()
elif ok == False:
    print('Not adding song to playlist.')
    search()



print('Finished')